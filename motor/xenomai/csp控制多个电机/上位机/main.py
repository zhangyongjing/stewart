import matplotlib
matplotlib.use("Qt5Agg")  # 声明使用QT5
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
import matplotlib.pyplot as plt
from PyQt5 import QtWidgets,QtCore
from PyQt5.QtWidgets import QMainWindow,QApplication
from mpl_toolkits.mplot3d import Axes3D 
from mpl_toolkits.mplot3d.art3d import Poly3DCollection,Line3DCollection

import sys
import Ui_stewart
class Figure_Canvas(FigureCanvas):   # 通过继承FigureCanvas类，使得该类既是一个PyQt5的Qwidget，又是一个matplotlib的FigureCanvas，这是连接pyqt5与matplotlib的关键
    def __init__(self):
        fig =plt.Figure(figsize=(5, 5))  # 创建一个Figure，注意：该Figure为matplotlib下的figure，不是matplotlib.pyplot下面的figure
        FigureCanvas.__init__(self, fig) # 初始化父类
        #self.setParent(parent)
        #参数一子图总行数，参数二子图总列数，参数三子图位置
        self.ax = Axes3D(fig)
        self.ax.set_xlabel('x')
        self.ax.set_ylabel("Y")
        self.ax.set_zlabel("Z")
        self.ax.set_xbound(lower=-60,upper=60)#轴坐标的范围
        self.ax.set_ybound(lower=-60,upper=60)
        self.ax.set_zbound(lower=0,upper=100)
        #隐藏掉x,y,z刻度轴
        self.ax.set_xticks([])
        self.ax.set_yticks([])
        self.ax.set_zticks([])
        #去掉坐标轴
        self.ax.axis("off")
    
    def test(self,x,y,z,color):  
        verts = [list(zip(x,y,z))]#zip讲列表打包成元组，list讲元组转化成列表 
        tri=Poly3DCollection(verts)
        tri.set_facecolor(color)
        self.ax.add_collection3d(tri)
        self.ax.set_facecolor("w")
        print(verts)
    def line(self,x_u,y_u,z_u,x_l,y_l,z_l,i):
        line=[[(x_u[i],y_u[i],z_u[i]),(x_l[i],y_l[i],z_l[i])]]
        line1=Line3DCollection(line)
        self.ax.add_collection3d(line1) 

class pictur(QMainWindow,Ui_stewart.Ui_MainWindow): 
    x_u=[ -25.25,  25.25, 38.25,13,    -13,-38.25]
    y_u=[ 29.59, 29.59, 7.07,-36.67, -36.67, 7.08 ]
    z_u=[102.81, 102.81, 102.81, 102.81, 102.81, 102.81]
    x_l=[ -15, 15, 59.46 ,44.46, -44.46, -59.46]
    y_l=[ 60, 60, -17.01,-42.99, -42.99, -17.01 ]
    z_l=[0,0,0,0,0,0]
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        dr=Figure_Canvas()
        dr.test(self.x_u,self.y_u,self.z_u,"#7CFC00")
        dr.test(self.x_l,self.y_l,self.z_l,"#836FFF")
        for i in range(6):
            dr.line(self.x_u,self.y_u,self.z_u,self.x_l,self.y_l,self.z_l,i)
            
        self.GraphicsScene=QtWidgets.QGraphicsScene()
        self.GraphicsScene.addWidget(dr)
        self.graphicsView.setScene(self.GraphicsScene)
        self.graphicsView.show
if __name__ == '__main__':
    QtCore.QCoreApplication.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling)
    app = QApplication(sys.argv)
    zyj =pictur()
    zyj.show()
    sys.exit(app.exec_())
